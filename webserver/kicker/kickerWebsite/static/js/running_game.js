// global websocket message
var socket;

// when Alpine is ready open websocket connection
document.addEventListener('alpine:init', () => {

    // init store
    Alpine.store('game', {
        running: true,
        r: {
            name: document.getElementById('name-red').innerText,
            score: document.getElementById('score-red').innerText,
        },
        b: {
            name: document.getElementById('name-blue').innerText,
            score: document.getElementById('score-blue').innerText,
        },
        updateScore(scoreRed, scoreBlue) {
            this.r.score = scoreRed;
            this.b.score = scoreBlue;
        },
        endGame() {
            if (this.running) {
                this.running = false;
            }
        }
    });
    Alpine.store('connected', false);
    Alpine.store('poll', {
        status: "ended",
        type: "",
        result: "",
        numPollers: 0,
        votes: {
            yes: 0,
            no: 0,
        },
        // updates the state in the store according to the data it gets
        updatePoll(pollData) {
            if (this.status === 'ended') {
                this.status = 'polling';
            }
            this.type = pollData.type;
            this.numPollers = pollData.num_pollers;
            this.votes.yes = pollData.yes;
            this.votes.no = pollData.no;
        },
        // sets the state in the store so that the poll is ended
        endPoll(result) {
            this.status = "ended";
            this.result = result;
        }
    });


    // websocket
    socket = new WebSocket('ws://' + window.location.host);

    socket.onopen = (evt) => {
        Alpine.store('connected', true);
    };

    socket.onclose = (evt) => {
        Alpine.store('connected', false)
    }

    socket.onmessage = (evt) => {
        let msg = JSON.parse(evt.data)
        switch (msg.type) {
            case "updatescore":
                Alpine.store('game').updateScore(msg.score_red, msg.score_blue);
                break;

            case "startpoll":
            case "updatepoll":
                Alpine.store('poll').updatePoll(msg.poll_data);

                // draw poll bar
                const percent_width_yes = msg.poll_data.yes * 100 / msg.poll_data.num_pollers;
                const percent_width_no = msg.poll_data.no * 100 / msg.poll_data.num_pollers;

                document.getElementById("yes-bar").style.width = percent_width_yes.toString()+"%";
                document.getElementById("no-bar").style.width = percent_width_no.toString()+"%";
                break;

            case "endpoll":
                Alpine.store('poll').endPoll(msg.result);
                if (msg.poll_type === 'endgame' && msg.result === 'approved') {
                    Alpine.store('game').endGame();
                }
                break;
            case "error":
                var friendlyErrMsg;
                // set user friendly error msg according to system msg
                switch (msg.error_message) {
                    case "poll already running":
                        friendlyErrMsg = "Es wurde bereits eine Poll gestartet. Solange diese läuft, ist es nicht möglich eine weitere zu starten."
                        break;
                    default:
                        friendlyErrMsg = "Unbekannter Fehler.";
                }
                showNewAlert(friendlyErrMsg);
            break;
        }
    };
})

const game = () => {
    return {
        // functio to start a poll
        changeScore(team, change) {
            const score = Alpine.store('game');
            const teamScore = score[team].score;

            // don't start to decrease score if already 0
            if (change === "-" && teamScore === 0) {
                return
            }
            // build string to send to backend
            // team: r or b
            // change: + or -
            const type = 'changescore' + team + change + '1';
            startPoll(type);
        },
        closeGame() {
            startPoll('endgame');
        }
    }
}

const poll = () => {
    return {
        voted: false,
        closed: true,
        msg: "",
        // create bootstrap modal object to be able to open the modal from code
        bModal: new bootstrap.Modal(document.getElementById('poll-modal')),

        init() {
            this.$watch('$store.poll.status', (status) => {
                this.voted = false;
                this.closed = false;
                if (status == 'polling') {
                    // open modal
                    this.bModal.show();
                }
                if (status === 'ended') {
                    const poll = Alpine.store('poll')
                    const team_name = Alpine.store('game')[poll.type[11]].name;
                    const change = poll.type[12] === "+" ? "erhöht" : "verringert";
                    const result = poll.result === "approved" ? " " : " nicht ";
                    this.msg = `Der Spielstand des Teams "${team_name}" wurde${result}um 1 Tor ${change}.`;
                }
            });

            this.$watch('$store.poll.type', (type) => {
                if (type === "endgame") {
                    this.msg = "Spiel beenden?"
                } else if (type.startsWith("changescore")) {
                    // get corresponding team name => type[11] is b(lue) or r(ed)
                    const team_name = Alpine.store('game')[type[11]].name;
                    const change = type[12] === "+" ? "erhöhen" : "verringern";
                    this.msg = `Den Spielstand des Teams "${team_name}" um 1 Tor ${change}?`;
                }
            })
        },
        vote(vote) {
            changePoll(vote === "yes");
            this.voted = true;
        },
    }
}


/*################################################
#   websocket send functions according to api    #
################################################*/

const startPoll = pollType => {
    socket.send(JSON.stringify({
        type: 'pollstart',
        poll_type: pollType
    }))
}

const changePoll = vote => {
    // vote is true, if the player voted to change the score
    socket.send(JSON.stringify({
        type: 'pollchange',
        yes: vote,
        no: !vote
    }))
}


// create bootstrap alert dom element 
const bootstrapAlert = {
    create(message, color) {
        var alertDiv = document.createElement('div');
        alertDiv.classList = `alert alert-${color} alert-dismissible fade show`;
        alertDiv.ariaRoleDescription = 'alert';
        var closeButton = document.createElement('button');
        closeButton.type = 'button';
        closeButton.classList = ['btn-close'];
        closeButton.ariaLabel = "Close";
        closeButton.setAttribute('data-bs-dismiss', 'alert');
        alertDiv.textContent = message;
        alertDiv.appendChild(closeButton);
        return alertDiv;
    },
    danger(message) {
        return this.create(message, "danger")
    },
    warning(message) {
        return this.create(message, "warning");
    },
    info(message) {
        return this.create(message, "info");
    },
    light(message) {
        return this.create(message, "light");
    },
    transparent(message) {
        return this.create(message, "transparent");
    },
}

// handle error alerts in dom 
const showNewAlert = msg => {

    var alertList = document.getElementById("alert-list");

    // only show 3 alerts at a time
    if (alertList.childElementCount >= 3) {
        // remove oldest error alert
        alertList.removeChild(alertList.childNodes[1]);
    }
    // create new bootstrap alert element
    var alertNode = bootstrapAlert.warning(msg);
    // and append it to the alert list
    alertList.appendChild(alertNode);
}