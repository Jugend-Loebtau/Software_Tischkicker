from django.db import models


# A single Player has simply a name
class Player(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


# A Team has a name and consists of players
class Team(models.Model):
    name = models.CharField(max_length=30)
    players = models.ManyToManyField(Player)

    def __str__(self):
        return self.name


# In a Leaderboard there can be several Teams competing against each other
# Types of Leaderboards may be:
# open(Teams can join at any time),
# closed(no joining of teams after creation of the Leaderboard), is this needed?
# tournament(closed + fixed sequence and number of games to play-->every Team against every Team)
class Leaderboard(models.Model):
    name = models.CharField(max_length=30)
    teams = models.ManyToManyField(Team)  # only closed or tournament Leaderboards need dedicated Teams, no teams means every team can play games in it
    type = models.CharField(max_length=30)  # normal, tournament
    info = models.CharField(max_length=100)

    def __str__(self):
        return self.name


# A Game contains the running bool, the competing Teams, their scores, the Leaderboard it belongs to
class Game(models.Model):
    running = models.BooleanField(default=True)
    team_red = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='games_team_red')
    team_blue = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='games_team_blue')
    score_red = models.IntegerField()
    score_blue = models.IntegerField()
    leaderboard = models.ForeignKey(Leaderboard, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.team_blue) + ' vs. ' + str(self.team_red)
