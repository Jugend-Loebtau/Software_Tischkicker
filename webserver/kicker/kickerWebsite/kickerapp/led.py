import time
try:
    import RPi.GPIO as GPIO
except ImportError:
    # mock GPIO for development
    import Mock.GPIO as GPIO


LedPin = 11


def setup():
    GPIO.setmode(GPIO.BOARD)        # Numbers GPIOs by physical location
    GPIO.setup(LedPin, GPIO.OUT)    # Set LedPin's mode is output
    GPIO.output(LedPin, GPIO.HIGH)  # Set LedPin high(+3.3V) to turn on led


def blink():
    while True:
        GPIO.output(LedPin, 1)  # led on
        time.sleep(1)
        GPIO.output(LedPin, 0)  # led off
        time.sleep(1)


def destroy():
    GPIO.output(LedPin, GPIO.LOW)   # led off
    GPIO.cleanup()                  # Release resource


def goal():
    pass


def winning_goal():
    pass


