from django.apps import AppConfig
from django.core.cache import cache

class KickerappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kickerapp'
    def ready(self):
        # code for startup, initialize cache variable for tracking number of ws clients
        cache.set('num_ws_clients', 0)
        # the 'poll' key will only be added (cache.add('poll', value)) when a poll is started and will be deleted when it is complete
        # 'poll': {'type': 'changescore/endgame', 'num_users': (num_ws_clients), 'yes': Int, 'no': Int}

