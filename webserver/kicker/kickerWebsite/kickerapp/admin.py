from django.contrib import admin

from .models import Player, Team, Leaderboard, Game

admin.site.register(Player)
admin.site.register(Team)
admin.site.register(Leaderboard)
admin.site.register(Game)