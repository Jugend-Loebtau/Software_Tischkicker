from django.shortcuts import render, redirect
from .models import Game, Team, Player, Leaderboard
from django.core.exceptions import ObjectDoesNotExist


def home(request):
    try:
        game = Game.objects.get(running=True)

        context = {'rname': game.team_red, 'bname': game.team_blue, 'rscore': game.score_red, 'bscore': game.score_blue}

        return render(request, 'kickerapp/running_game.html', context=context)

    except ObjectDoesNotExist:
        return render(request, 'kickerapp/start_game.html')


def stats(request, leaderboard=None):
    if leaderboard is None:
        context = {
            'leaderboards': [lb.name for lb in Leaderboard.objects.all()]
        }
        return render(request, 'kickerapp/stats_home.html', context=context)
    else:
        try:
            lb = Leaderboard.objects.get(name=leaderboard)
            context = {
                'name': lb.name,
                'teams': [tm.name for tm in lb.teams.all()],
                'type': lb.type,
                'info': lb.info,
                'num_games': lb.game_set.count(),
                'num_teams': lb.teams.count(),
                'num_players': sum([tm.players.count() for tm in lb.teams.all()]),
                'teams_by_victories': sorted([{'name': tm.name, 'victories': sum([1 for gm in lb.game_set.all() if gm.team_red.name == tm.name and gm.score_red == 10]+[1 for gm in lb.game_set.all() if gm.team_blue.name == tm.name and gm.score_blue == 10])} for tm in lb.teams.all()], key=getVicts, reverse=True),
                'teams_by_totalscore': sorted([{'name': tm.name, 'totalscore': sum([gm.score_red for gm in lb.game_set.filter(team_red=tm.id)]+[gm.score_blue for gm in lb.game_set.filter(team_blue=tm.id)])} for tm in lb.teams.all()], key=getScores, reverse=True),
                'teams_by_totalgames': sorted([{'name': tm.name, 'totalgames': sum([1 for gm in lb.game_set.filter(team_red=tm.id)]+[1 for gm in lb.game_set.filter(team_blue=tm.id)])} for tm in lb.teams.all()], key=getGames, reverse=True),
                'teams_by_totalscorediffs': sorted([{'name': tm.name, 'totalscorediffs': sum([gm.score_red - gm.score_blue for gm in lb.game_set.filter(team_red=tm.id)]+[gm.score_blue - gm.score_red for gm in lb.game_set.filter(team_blue=tm.id)])} for tm in lb.teams.all()], key=getDiffs, reverse=True),
            }

            return render(request, 'kickerapp/stats.html', context=context)
        except ObjectDoesNotExist:
            return redirect('home')


def getVicts(team):
    return team.get('victories')

def getScores(team):
    return team.get('totalscore')

def getGames(team):
    return team.get('totalgames')

def getDiffs(team):
    return team.get('totalscorediffs')
