import threading
# import asyncio
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
import random
from channels.generic.websocket import WebsocketConsumer
from .models import Game, Team, Leaderboard, Player
from . import kickerfuncs
import json
from asgiref.sync import async_to_sync
from django.core.cache import cache
from django.core.serializers import serialize

class kickerConsumer(WebsocketConsumer):
    def connect(self):
        print('connected')
        # put every connection into watching group
        async_to_sync(self.channel_layer.group_add)('kickerclients', self.channel_name)

        self.polling = False

        old_num = cache.get('num_ws_clients')
        # sometimes num_ws_clients is not set in cache
        if old_num is None:
            old_num = 0
        cache.set('num_ws_clients', old_num+1)
        self.accept()

    def receive(self, text_data):
        print('received', text_data)
        # Data from the Frontend to the Backend:
        # either starting new game or manipulating the score
        data = json.loads(text_data)

        # from startgame
        if data['type'] == 'newplayers':
            for player in data['players']:
                pl = Player.objects.create(name=player['name'])

        if data['type'] == 'newteams':
            for team in data['teams']:
                tm = Team.objects.create(name=team['name'])
                for player in team['players']:
                    tm.players.add(Player.objects.get(name=player))

        if data['type'] == 'newleaderboard':
            lb = Leaderboard.objects.create(name=data['name'], type=data['ldb_type'], info=data['info'])
            for team in data['teams']:
                lb.teams.add(Team.objects.get(name=team))

        if data['type'] in ['newplayers', 'newteams', 'newleaderboard']:
            kickerdata = {
                'leaderboards': [{'name': lb.name, 'teams': [tm.name for tm in lb.teams.all()]} for lb in Leaderboard.objects.all()],
                'teams': [{'name': tm.name, 'players': [pl.name for pl in tm.players.all()]} for tm in Team.objects.all()],
                'players': [pl.name for pl in Player.objects.all()],
            }
            # send kickerdata to all clients
            async_to_sync(self.channel_layer.group_send)(
                'kickerclients',
                {
                    'type': 'send.kickerdata',
                    'kicker_data': kickerdata
                },
            )
        
        if data['type'] == 'getkickerdata':
            kickerdata = {
                'leaderboards': [{'name': lb.name, 'teams': [tm.name for tm in lb.teams.all()]} for lb in Leaderboard.objects.all()],
                'teams': [{'name': tm.name, 'players': [pl.name for pl in tm.players.all()]} for tm in Team.objects.all()],
                'players': [pl.name for pl in Player.objects.all()],
            }

            # send kickerdata to requesting client
            self.send(json.dumps({
                'type': 'kickerdata',
                'kicker_data': kickerdata
            }))

        if data['type'] == 'startgame':

            kickerfuncs.startgame(data['team_red'], data['team_blue'], data['leaderboard'])

        # from runninggame
        if data['type'] == 'pollstart':
            try:
                g = Game.objects.get(running=True)
            except ObjectDoesNotExist:
                g = None
            poll = cache.get('poll')

            if poll is None and g is not None:
                poll = {
                    'type': data['poll_type'],
                    'num_pollers': cache.get('num_ws_clients'),
                    'yes': 0,
                    'no': 0,
                }
                cache.set('poll', poll)  # data['poll_data'])

                async_to_sync(self.channel_layer.group_send)(
                    'kickerclients',
                    {
                        'type': 'send.startpoll',
                        'poll_data': cache.get('poll')
                    },
                )
            else:
                self.send(json.dumps({
                    'type': 'error',
                    'error_message': 'poll already running'
                }))

        if data['type'] == 'pollchange':
            poll = cache.get('poll')
            if data['yes']:
                poll['yes'] += 1
            elif data['no']:
                poll['no'] += 1

            cache.set('poll', poll)
            self.polling = False
            async_to_sync(self.channel_layer.group_send)(
                'kickerclients',
                {
                    'type': 'send.updatepoll',
                    'poll_data': cache.get('poll')
                },
            )

            self.check_poll_complete()

    def send_startedgame(self, event):
        self.send(json.dumps({
            'type': 'startedgame',
            'game_data': event['game_data']
        }))

    def send_updatescore(self, event):
        self.send(json.dumps({
            'type': 'updatescore',
            'score_red': event['score_red'],
            'score_blue': event['score_blue']
        }))

    def send_kickerdata(self, event):
        self.send(json.dumps({
            'type': 'kickerdata',
            'kicker_data': event['kicker_data']
        }))

    def send_startpoll(self, event):
        self.polling = True
        self.send(json.dumps({
            'type': 'startpoll',
            'poll_data': event['poll_data'],
        }))

    def send_updatepoll(self, event):
        self.send(json.dumps({
            'type': 'updatepoll',
            'poll_data': event['poll_data'],
        }))

    def send_endpoll(self, event):
        self.polling = False
        self.send(json.dumps({
            'type': 'endpoll',
            'poll_type': event['poll_type'],
            'result': event['result']
        }))

    def check_poll_complete(self):
        poll = cache.get('poll')

        poll_is_complete = poll['yes'] > (poll['num_pollers'] / 2) or \
                           poll['no'] > (poll['num_pollers'] / 2) or \
                           (poll['yes'] + poll['no']) == poll['num_pollers']

        if poll_is_complete:
            cache.delete('poll')
            if poll['yes'] > poll['no']:
                async_to_sync(self.channel_layer.group_send)(
                    'kickerclients',
                    {
                        'type': 'send.endpoll',
                        'poll_type': poll['type'],
                        'result': 'approved',
                    },
                )

                if poll['type'].startswith('changescore'):
                    kickerfuncs.changescore(poll['type'][11], int(poll['type'][12:]))

                elif poll['type'] == 'endgame':
                    kickerfuncs.endgame()
            else:
                async_to_sync(self.channel_layer.group_send)(
                    'kickerclients',
                    {
                        'type': 'send.endpoll',
                        'poll_type': poll['type'],
                        'result': 'rejected'
                    },
                )

    def disconnect(self, close_code):
        print('disconnected', close_code)
        async_to_sync(self.channel_layer.group_discard)('kickerclients', self.channel_name)
        poll = cache.get('poll')
        if poll is not None and self.polling:
            poll['no'] += 1
            cache.set('poll', poll)
            self.polling = False

            async_to_sync(self.channel_layer.group_send)(
                'kickerclients',
                {
                    'type': 'send.updatepoll',
                    'poll_data': cache.get('poll')
                },
            )

            self.check_poll_complete()

        old_num = cache.get('num_ws_clients')
        cache.set('num_ws_clients', old_num-1)
