from .models import Game, Team, Leaderboard
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
import threading
import random
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.cache import cache
from . import scoreDisplay
try:
    import RPi.GPIO as GPIO
except ImportError:
    # mock GPIO for development
    import Mock.GPIO as GPIO

tor_rot = 33
tor_blau = 12
GPIO.setup(tor_rot, GPIO.IN)
GPIO.setup(tor_blau, GPIO.IN)
GPIO.setmode(GPIO.BOARD)

def startgame(team_red, team_blue, leaderboard):
    # create new game in the database
    g = Game.objects.create(
        running=True,
        team_red=Team.objects.get(name=team_red),
        team_blue=Team.objects.get(name=team_blue),
        score_red=0,
        score_blue=0,
        leaderboard=Leaderboard.objects.get(name=leaderboard)
    )

    t = threading.Thread(target=listen_goal)
    t.start()

    # send a message to all clients that a new game has started
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        'kickerclients',
        {
            'type': 'send.startedgame',
            'game_data': [g.team_red.name, g.team_blue.name, g.leaderboard.name]
        },
    )



def endgame():
    g = Game.objects.get(running=True)
    if g.score_red == 10 or g.score_blue == 10:
        g.running = False
        g.save()
    else:
        g.delete()  # should not affect the python object, only the database

    if g.leaderboard.type == 'tournament':
        # if there are still games to play in the tournament
        if len(g.leaderboard.game_set.all()) < sum(range(1,len(g.leaderboard.teams.all()))):

            team_red = random.choice([tm.name for tm in g.leaderboard.teams.all()])
            # while team_red already has played all its games
            while len(g.leaderboard.game_set.filter(Q(team_red=team_red) | Q(team_blue=team_red))) >= len(g.leaderboard.teams.all()) - 1:
                team_red = random.choice([tm.name for tm in g.leaderboard.teams.all()])

            team_blue = random.choice([tm.name for tm in g.leaderboard.teams.all()])
            # while team_blue already played against team_red
            while team_blue in [team_name for gm in g.leaderboard.game_set.filter(Q(team_red=team_red) | Q(team_blue=team_red)) for team_name in [gm.team_red.name, gm.team_blue.name]]:
                team_blue = random.choice([tm.name for tm in g.leaderboard.teams.all()])

            startgame(team_red, team_blue, g.leaderboard.name)


def listen_goal():
    GPIO.add_event_detect(tor_rot, GPIO.RISING, callback=lambda: changescore('r', +1), bouncetime=50)
    GPIO.add_event_detect(tor_blau, GPIO.RISING, callback=lambda: changescore('b', +1), bouncetime=50)

def changescore(team, quantity):
    try:
        g = Game.objects.get(running=True)
    except ObjectDoesNotExist:
        g = None
    poll = cache.get('poll')

    # at polling time scoring goals is deactivated
    if poll is None and g is not None:
        scorechanged = False
        if team == 'r':
            if g.score_red + quantity >= 0 and g.score_red + quantity <= 10:
                g.score_red += quantity
                scorechanged = True
        elif team == 'b':
            if g.score_blue + quantity >= 0 and g.score_blue + quantity <= 10:
                g.score_blue += quantity
                scorechanged = True

        g.save()

        if scorechanged:
            # update the sevensegment
            scoreDisplay.displayScore7Segment(score=g.score_red, team=0)
            scoreDisplay.displayScore7Segment(score=g.score_blue, team=1)

            channel_layer = get_channel_layer()

            async_to_sync(channel_layer.group_send)(
                "kickerclients",
                {
                    "type": "send.updatescore",
                    "score_red": g.score_red,
                    "score_blue": g.score_blue,
                },
            )

            # check if a team has won
            if g.score_red >= 10 or g.score_blue >= 10:
                poll = cache.set('poll', {'type': 'endgame', 'num_pollers': cache.get('num_ws_clients'), 'yes': 0, 'no': 0})

                async_to_sync(channel_layer.group_send)(
                    "kickerclients",
                    {
                        "type": "send.startpoll",
                        "poll_data": poll,
                    },
                )

            # make LEDs blink
            #     ======   ======   ======  ========\\    //\\    //\\    // ||
            #    ||    || ||    || ||    ||    ||    \\  //  \\  //  \\  //  ||
            #    ||====|| ||====|| ||====||    ||     \\//    \\//    \\//   ||
            #    ||       ||    || || \\       ||      ||      ||      ||    ||
            #    ||       ||    || ||  \\      ||      ||      ||      ||    ()
