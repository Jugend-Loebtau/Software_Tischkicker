from django.urls import path

from .consumers import kickerConsumer

ws_urlpatterns = [
    path('', kickerConsumer.as_asgi())
]