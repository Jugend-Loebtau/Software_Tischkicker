# Django Web app
## Installation (Linux)
1. Install virtualenv on your system
- e.g. via pip:
```shell 
$ pip install --user virtualenv
```
2. change dir to ```kicker```
```shell
$ cd kicker
```
3. create a new venv environment in this directory
```shell
$ virtualenv venv
```
4. activate the virtual environment
```shell
$ source venv/bin/activate
```
- now you are in an virtual environment and everything you install using pip is only going to be installed there and not for your whole system
5. install all requirements
```shell 
$ pip install -r requirements.txt
```

## Run this app
1. activate virtual environment
```shell
$ source venv/bin/activate
```
2. change dir to `kickerWebsite`
```shell
$ cd kicker/kickerWebsite
```
3. start django dev server
```shell
$ python3 manage.py runserver
```
4. open your browser and navigate to ```localhost:8000```
