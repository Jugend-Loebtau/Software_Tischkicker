import time
import RPi.GPIO as GPIO

################
# Declarations #
################

# for Pi Model 3B V1.1  [[P1e_LE, P1z_LE],[P2e_LE, P2z_LE]]
PIN_SET_TEN_RED = 17
PIN_SET_UNIT_RED = 15

PIN_SET_TEN_BLUE = 21
PIN_SET_UNIT_BLUE = 18


#set_pins = [[ 7, 6],[ 9, 8]]   # for Pi Model 3B V1.1  [[P1z_LE, P1e_LE],[P2z_LE, P2e_LE]]
set_pins = [
  [PIN_SET_TEN_RED, PIN_SET_UNIT_RED],
  [PIN_SET_TEN_BLUE, PIN_SET_UNIT_BLUE]
  ]   

#bcd_out_pins = [ 2, 3, 4, 5]        # for Pi Model 3B V1.1  (MSB first)
bcd_out_pins = [ 0, 1, 4,14]        # for Pi model B V1.0   (MSB first)

#########
# Setup #
#########

# use BCM Layout
GPIO.setmode(GPIO.BCM)

# setup pins
for pin in bcd_out_pins + set_pins:
  GPIO.setup(pin, GPIO.OUT)
  GPIO.output(pin, GPIO.LOW)
  
#############
# Functions #
#############
  
def getBCDCodeAsList(place):
  binary = bin(int(place))
  #get binaries in list and remove first 0b
  binary_list = list(binary)[2:]
  # insert leading 0 while not 4 bits long
  while len(binary_list) != 4:
    binary_list.insert(0, '0')
  return binary_list

# function to display score on 7 segment displays
def displayScore7Segment(score, team):
  current_set_pins = set_pins[team] # pins to set unit and tens of current display e.g [15, 17]
  
  score_places = list(str(score)) # e.g 10 = ['1','0']
  # add leading 0 if only unit e.g. ['8']
  if (len(score_places) == 1):
    score_places.insert(0, '0')
  
  # display place to the two displays for ten and unit
  for place_id, place_val in enumerate(score_places):
    # get bcd coding for current place (unit/ten)
    bcd_code_place = getBCDCodeAsList(place_val)
    print(bcd_code_place)
    
    # output bcd code for value to pins
    for idx, bit in enumerate(bcd_code_place):
     GPIO.output(bcd_out_pins[idx], bit)
     
    # display current place on team display
    GPIO.output(current_set_pins[place_id], GPIO.HIGH)
    time.sleep(0.02)
    GPIO.output(current_set_pins[place_id], GPIO.LOW)
