#!/usr/bin/env python

import time
import RPi.GPIO as GPIO
from producer import produce
import asyncio

Counter = 0

def goal():
    global Counter
    Counter += 1
    asyncio.run(produce(message=f"Score: {Counter}", host='192.168.178.62', port=4000))
    print(Counter)

def main():

    # tell the GPIO module that we want to use the 
    # chip's pin numbering scheme
    GPIO.setmode(GPIO.BCM)

    # setup pin 25 as an output
    GPIO.setup(23,GPIO.IN)
    score = 0
    GPIO.add_event_detect(23, GPIO.FALLING, callback=lambda x: goal(), bouncetime=400)
    


if __name__=="__main__":
    main()
    while True: 
        pass