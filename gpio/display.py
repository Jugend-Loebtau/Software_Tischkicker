from classes.side import Side
import RPi.GPIO as GPIO
import time

#(tens, unit)
control_pins = {Side.RIGHT: (26, 31), Side.LEFT: (21, 24)}
bcd_pins = [3, 5, 7, 29]

GPIO.setmode(GPIO.BOARD)
for pin in control_pins[Side.RIGHT]:
    GPIO.setup(pin, GPIO.OUT)

for pin in control_pins[Side.LEFT]:
    GPIO.setup(pin, GPIO.OUT)

for pin in bcd_pins:
    GPIO.setup(pin, GPIO.OUT)


def setDisplay(side, goals):
    # slice goals for the two displays
    # e.g. 12 => unit = 2, tens = 10
    unit = goals % 10
    tens = int(goals / 10) % 10

    # don't display zero on the tens display
    # => set it to invalid input e.g. 10
    if(tens == 0):
        tens = 10

    # encode parts to bcd codes
    bcd = [format(tens, '04b'), format(unit, '04b')]
    print(bcd)

    # set both displays
    # first tens is set and second unit is set
    for part, control_pin in enumerate(control_pins[side]):
        print("Now setting up display on control pin " + str(control_pin))
        # for every digit in the bcd code set the specific pin
        for i, state in enumerate(bcd[part]):
            GPIO.output(bcd_pins[i], int(state))
            print("Pin " + str(bcd_pins[i]) + " is set to " + state)
        # send signal to finally update display
        GPIO.output(control_pin, 0)
        time.sleep(0.1)
        GPIO.output(control_pin, 1)


def resetDisplays():
    setDisplay(Side.LEFT, 0)
    setDisplay(Side.RIGHT, 0)
