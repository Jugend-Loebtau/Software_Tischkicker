from gpio.display import setDisplay

class Team:
    def __init__(self, team_side, team_name):
        self.__side = team_side
        self.__name = team_name
        self.__goals = 0

    def incrGoals(self):
        self.__goals += 1
        setDisplay(self.__side, self.__goals)
        return self.__goals

    def getGoals(self):
        return self.__goals
    
    def getName(self):
        return self.__name